package main

import (
	"os"

	"gitlab.com/thesepehrm/simplepos/cmd/simpleposd/cmd"
)

func main() {
	rootCmd, _ := cmd.NewRootCmd()
	if err := cmd.Execute(rootCmd); err != nil {
		os.Exit(1)
	}
}
